# Shiba Book Shop

### Project setup

### YARN
```
yarn
```
### NPM
```
npm run install
```

### Compiles and hot-reloads for development

### YARN
```
yarn serve
```
### NPM
```
npm run serve
```

## Compiles and minifies for production

### YARN
```
yarn build
```
### NPM
```
npm run build
```