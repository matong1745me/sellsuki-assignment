export default {
  namespaced: true,
  state: {
    data: [],
  },
  mutations: {
    addBookToCart: (state, payload) => {
      let found = false
      state.data = state.data.map((book) => {
        if (book.id === payload.id) {
          found = true
          return {
            ...book,
            amount: book.amount + 1,
          }
        }
        return book
      })
      if (!found) {
        state.data = [
          ...state.data,
          {
            ...payload,
            amount: 1,
          },
        ]
      }
    },
    addBookById: (state, { id }) => {
      state.data = state.data.map((book) => {
        if (book.id === id) {
          return {
            ...book,
            amount: book.amount + 1,
          }
        }
        return book
      })
    },
    removeBookById: (state, { id }) => {
      state.data = state.data.map((book) => {
        if (book.id === id) {
          return {
            ...book,
            amount: book.amount - 1,
          }
        }
        return book
      })
    },
    resetCart: (state) => {
      state.data = []
    },
  },
  actions: {
  },
  getters: {
    filterCart: (state) => state.data.filter((book) => book.amount > 0),
  },
}
