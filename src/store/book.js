import axios from 'axios'

const api = 'https://api.jsonbin.io/b/5e69b564d2622e7011565547'

export default {
  namespaced: true,
  state: {
    data: [],
  },
  mutations: {
    setBooks(state, payload) {
      state.data = payload
    },
  },
  actions: {
    async fetchBooks({ commit }) {
      await axios.get(api)
        .then((res) => commit('setBooks', res.data.books))
    },
  },
}
