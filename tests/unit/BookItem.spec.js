import { shallowMount } from '@vue/test-utils'
import BookItem from '@/components/BookItem.vue'

let wrapper = null

afterEach(() => {
  wrapper.destroy()
})

describe('BookItem.vue', () => {
  it('renders props.data when passed', () => {
    const data = {
      title: 'Test',
      price: 100,
      cover: 'Image url',
    }
    wrapper = shallowMount(BookItem, {
      propsData: { data },
    })
    expect(wrapper.find('.title').text()).toContain(data.title)
    expect(wrapper.find('.price').text()).toMatch(`฿${data.price}`)
  })
})
