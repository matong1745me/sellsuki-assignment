import { shallowMount } from '@vue/test-utils'
import HistoryBookItem from '@/components/HistoryBookItem.vue'

let wrapper = null

afterEach(() => {
  wrapper.destroy()
})

describe('HistoryBookItem.vue', () => {
  it('renders props.data and index when passed', () => {
    const data = {
      title: 'Test',
      price: 100,
      cover: 'Image url',
      amount: 2,
    }
    wrapper = shallowMount(CartItem, {
      propsData: { data },
    })
    expect(wrapper.find('.title').text()).toMatch(data.title)
  })
})
